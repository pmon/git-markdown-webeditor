#!/bin/bash
usage='release major|minor|patch'
if [ -n "$1" ]
then
  echo "npm version $1"
  npm version $1
  echo "npm publish"
  npm publish
else
  echo $usage
fi