// git-md-webedit, copyright (c) by Philippe Monnaie and others
// Distributed under a GPL 3.0 license: https://gitlab.com/pmon/git-markdown-webeditor/LICENSE.txt

"use strict";

var SimpleMDE = require('simplemde/src/js/simplemde.js');
var Cookies = require('js-cookie/src/js.cookie.js');
var gitlab = require('git-api-frontend/src/gitlab-api.js');
	
/* TODO
	Images/media (allow upload and automatically save them somewhere)
	Basic default screen locking (preferably without external dependencies)
*/
	
var git_md_webedit = function(options={}){

	var _replaceSelection = function(cm, active, startEnd, url) {
		//if(/editor-preview-active/.test(cm.getWrapperElement().lastChild.className))
		//	return;

		if(simplemde.isPreviewActive()){
			return;
		}
		
		var text;
		var start = startEnd[0];
		var end = startEnd[1];
		var startPoint = cm.getCursor("start");
		var endPoint = cm.getCursor("end");
		if(url) {
			end = end.replace("#url#", url);
		}
		if(active) {
			text = cm.getLine(startPoint.line);
			start = text.slice(0, startPoint.ch);
			end = text.slice(startPoint.ch);
			cm.replaceRange(start + end, {
				line: startPoint.line,
				ch: 0
			});
		} else {
			text = cm.getSelection();
			cm.replaceSelection(start + text + end);

			startPoint.ch += start.length;
			if(startPoint !== endPoint) {
				endPoint.ch += start.length;
			}
		}
		cm.setSelection(startPoint, endPoint);
		cm.focus();
	}
	
	var defaultSimpleMdeOptions = { 
		spellChecker: false ,
		autosave: false,
		toolbar: [
			"bold",
			"italic",
			"strikethrough",
			"heading",
			"|",
			"quote",
			"unordered-list",
			"ordered-list",
			"code",
			"|",
			//"link",
			{
				name: "internalLink",
				action: function(editor){
					var cm = editor.codemirror;
					var stat = simplemde.getState();
					getInteralLinkUrl( function(url){
						_replaceSelection(cm, stat.link, ["[", "](#url#)"], url);
					});
				},
				className: "fa fa-link",
				title: "Internal link"
			},
			{
				name: "externalLink",
				action: function(editor){
					var cm = editor.codemirror;
					var stat = simplemde.getState();
					getExteralLinkUrl( function(url){
						_replaceSelection(cm, stat.link, ["[", "](#url#)"], url);
					});
				},
				className: "fa fa-external-link",
				title: "External link"
			},
			//"image",
			{
				name: "image",
				action: function(editor){
					var cm = editor.codemirror;
					var stat = simplemde.getState();
					getImageUrl( function(url){
						_replaceSelection(cm, stat.image, ["![", "](#url#)"], url);
					});
				},
				className: "fa fa-picture-o",
				title: "Insert image"
			},
			"table",
			"|",
			"preview",
			"undo",
			"redo"
			//{
			//	name: "save",
			//	action: function save(editor){
			//			console.log("Save: TODO implement")
			//	},
			//	className: "fa fa-save",
			//	title: "Save",
			//}
		]
	}
	var defaultOptions = {
		giturl: 'https://gitlab.com/',
		loginExpiration: 365,
		editorOptions: defaultSimpleMdeOptions,
		pathUrlParam: 'page',
		linkId: 'link',
		editPanelId: 'edit_panel',
		loginButtonId: 'loginButton',
		logoutButtonId: 'logoutButton',
		commitButtonId: 'commitButton',
		rememberLoginCheckBoxId: 'rememberLogin',
		usernameInputId: 'username',
		passwordInputId: 'password',
		stayloggedinId: 'rememberLogin',
		commitMessageTextAreaId: 'commitMessage',
		visibleAuthenticatedClass: 'authenticated',
		visibleUnauthenticatedClass: 'unauthenticated',
		branchName: 'master',
		newpage: false,
		requireLogin: false,
		defaultContents: '',
		showMessageFunc: function(msg){
			alert(msg);
		},
		showErrorMessageFunc: function(msg){
			alert(msg);
		},
		defaultGitErrorCallBack: function(responseData, textStatus, errorThrown){
			console.log(textStatus);
			console.log(errorThrown);
		},
		urlPath2gitPath: function(path){
			return (path + '.md').replace("/.md",".md");
		},
		disableScreenFunc: function(){
			console.log('Screen disabled');
		},
		enableScreenFunc: function(){
			console.log('Screen enabled');
		},
		getImageUrl: function(callback){
			callback(prompt("Please enter the image url", "http://"));
		},
		getInteralLinkUrl: function(callback){
			callback(prompt("Please enter the url to link to", "/"));
		},
		getExteralLinkUrl: function(callback){
			callback(prompt("Please enter the url to link to", "http://"));
		}
	}

	var gitUrl = options.giturl || defaultOptions.giturl;
	var loginExpiration = options.loginExpiration || defaultOptions.loginExpiration;
	var pathUrlParam = options.pathUrlParam || defaultOptions.pathUrlParam;
	var projectPath = options.projectPath || defaultOptions.projectPath;
	var branchName = options.branchName || defaultOptions.branchName;
	var projectId = options.projectId || defaultOptions.projectId;
	var newpage = options.newpage || defaultOptions.newpage;
	var defaultContents = options.defaultContents || defaultOptions.defaultContents;
	var requireLogin = options.requireLogin || defaultOptions.requireLogin;
	var getImageUrl = options.getImageUrl || defaultOptions.getImageUrl;
	var getInteralLinkUrl = options.getInteralLinkUrl || defaultOptions.getInteralLinkUrl;
	var getExteralLinkUrl = options.getExteralLinkUrl || defaultOptions.getExteralLinkUrl;

	//TODO: set up defaults for editorOptions that are somehow more configurable (or would just merge?).
	var editorOptions = options.editorOptions || defaultOptions.editorOptions;
	var showErrorMessageFunc = options.showErrorMessageFunc || defaultOptions.showErrorMessageFunc;
	var showMessageFunc = options.showMessageFunc || defaultOptions.showMessageFunc;
	var defaultGitErrorCallbackFunc = options.defaultGitErrorCallBack || defaultOptions.defaultGitErrorCallBack;
	var urlPath2gitPath = options.urlPath2gitPath || defaultOptions.urlPath2gitPath
	var disableScreenFunc = options.disableScreenFunc || defaultOptions.disableScreenFunc
	var enableScreenFunc = options.enableScreenFunc || defaultOptions.enableScreenFunc
	var authenticatedElements;
	var unauthenticatedElements;
	var usernameField;
	var passwordField;
	var stayloggedinCheckBox;
	var editPanel;
	var commitMessageTextArea;
	var gl = gitlab(gitUrl);
	var userinfo;
	var simplemde;
	
	var getUrlParameter = function getUrlParameter(sParam) {
			var sPageURL = decodeURIComponent(window.location.search.substring(1)),
					sURLVariables = sPageURL.split('&'),
					sParameterName,
					i;
	
			for (i = 0; i < sURLVariables.length; i++) {
					sParameterName = sURLVariables[i].split('=');
	
					if (sParameterName[0] === sParam) {
							return sParameterName[1] === undefined ? true : sParameterName[1];
					}
			}
	};
	
	var getUrlPath = function(){
		return getUrlParameter(pathUrlParam);
	}
	
	var getNewPageInGitUrl = function(){
		var loc = gitUrl + projectPath + '/new/' + branchName + '/' + getRepositoryPath();
		loc = loc.replace("/.md",".md");
		return loc;
	};
	
	var getEditInGitUrl = function(){
		var loc = gitUrl + projectPath + '/edit/' + branchName + '/' + getRepositoryPath();
		loc = loc.replace("/.md",".md");
		return loc;
	};
	
	var getRepositoryPath = function getRepositoryPath(){
		return urlPath2gitPath(getUrlPath());
	};
	
	var getUser = function(){
		if(userinfo === undefined){
			var cookie = Cookies.get('user');
			if(cookie){
				userinfo = JSON.parse(cookie);
			}
		}
		return userinfo;
	}
	
	var setUser = function(user, stayloggedin){
		userinfo = user;
		if(stayloggedin){
			Cookies.set('user', user, { expires: loginExpiration });
		} else {
			Cookies.set('user', user);
		}
	}
	
	var commitSucceeded = function(result, textStatus, jqXHR){
		showMessageFunc('Changes saved and submitted');
		loadEditor();
	}
	
	var commitFailed = function(responseData, textStatus, errorThrown){
		showErrorMessageFunc('Commit failed with: ' + errorThrown);
		console.log(responseData)
		//commit failed, lets try to fork the project and then start a merge request? Or just show error?
		//fork project
		
		//commit changes to new project
		
		//merge request into original
		
		//if failed: defaultGitErrorCallbackFunc
		
		//delete project?
	}
	
	var commit = function(){
		//(projectId, filePath, contents, message, token, email, callback=defaultAjaxCallback, errorcallback=defaultGitErrorCallbackFunc)
		var filePath = getRepositoryPath();
		simplemde.toTextArea();
		var contents = editPanel.value;
		var message = getValueAndResetInputField(commitMessageTextArea);
		raw_commit(filePath, contents, message);
	}
	
	var raw_commit = function(filePath, contents, message, encoding){
		var user = getUser();
		if(user !== undefined){
			var token = user["private_token"];
			var email = user["email"];
			if(newpage){
				gl.commitNewFileToGit(projectId, filePath, contents, message, token, email, branchName,
					commitSucceeded,
					commitFailed,
					encoding
				)
			} else {
				gl.commitUpdateToGit(projectId, filePath, contents, message, token, email, branchName,
					commitSucceeded,
					commitFailed,
					encoding
				)
			}
		} else {
			showErrorMessageFunc('Git token needed for commit and not found, please log in with your git credentials using the form on the left');
		}	
	}
	
	var login = function login(user, password, stayloggedin, callback=loadEditor, errorcallback=showErrorMessageFunc){
		if(!user){
			var user = getValueAndResetInputField(usernameField);
		}
		if(!password){
			var password = getValueAndResetInputField(passwordField);
		}
		if(!stayloggedin){
			var stayloggedin = getValueAndResetCheckbox(stayloggedinCheckBox);
		}
		gl.loginGitLab(
			function(response){
				setUser(response, stayloggedin);
				setVisibility(true);
				callback();
			},
			function(responseData, textStatus, errorThrown){
				defaultGitErrorCallbackFunc(responseData, textStatus, errorThrown);
				errorcallback('Git login failed: ' + errorThrown);
			},
			user,
			password
		);
	}
	
	var logout = function logout(){
		Cookies.remove('user');
		userinfo = undefined;
		setVisibility(false);
	}
	
	var setVisibility = function setVisibility(authenticated){
		if(authenticated){
			authenticatedElements.forEach(function(item, index, arr){
				item[0].style.display = item[1];
			});
			unauthenticatedElements.forEach(function(item, index, arr){
				item[0].style.display = 'none';
			});
		} else {
			authenticatedElements.forEach(function(item, index, arr){
				item[0].style.display = 'none';
			});
			unauthenticatedElements.forEach(function(item, index, arr){
				item[0].style.display = item[1];
			});
		}
	}
	
	var loadEditor = function loadEditor(file_contents){
		if(file_contents){
			editorOptions['initialValue'] = file_contents;
			simplemde = new SimpleMDE(editorOptions);
		} else {
			loadEditorDefault()
		}
	}
	
	var loadEditorDefault = function(){
		var user = getUser();
		if(requireLogin && user == undefined){
			showErrorMessageFunc('Git token not found, please log in with your git credentials using the form on the left');
			return; //do not load things and just return
		}
		if(newpage){
			loadEditor(defaultContents);
		} else {
			
			if(user !== undefined){
				gl.getFileContents(
					projectId,
					getRepositoryPath(), 
					user.private_token,
					branchName,
					loadEditor,
					defaultGitErrorCallbackFunc
				);
			} else {
				gl.getFileContents(
					projectId,
					getRepositoryPath(), 
					undefined, //TODO deal with users that are not logged in (just leave the token param off, and maybe put it last as it's optional) 
					branchName,
					loadEditor,
					defaultGitErrorCallbackFunc
				);
			}
		}
	}
	
	var uploadFile = function(uploadPath, fileContents){ //expects the path as the path you would want your file to have in your site and the contents base64 encoded
		filePath = urlPath2gitPath(uploadPath);
		raw_commit(filePath, contents, message, 'base64');
		return uploadPath;
	}
	
	var registerClickHandler = function (element, handler){
		element.addEventListener("click", handler, false);
	}
	
	var setAttribute = function (element, attributename, attributevalue){
		element.setAttribute(attributename, attributevalue);
	}
	
	var getValueAndResetInputField = function(element){
		var returnValue = element.value;
		element.value = '';
		return returnValue;
	}
	
	var getValueAndResetCheckbox = function(element){
		var returnValue = element.checked;
		element.checked = false;
		return returnValue;
	}
	
	var getElementsFromOptions = function(name){
		var elements = [];
		if(options[name]){
			elements = options[name];
		} else {
			var elementClass = options[name+'Class'] || defaultOptions[name + 'Class'];
			elements = document.getElementsByClassName(elementClass);
		}
		return elements;
	}
	
	var getElementFromOptions = function(name){
		var elem;
		if(options[name]){
			elem = options[name];
		} else {
			var elemid = options[name+'Id'] || defaultOptions[name + 'Id'];
			elem = document.getElementById(elemid);
		}
		return elem;
	}
	
	var getElementAndDisplayStyle = function(element){
		return [element, element.style.display];
	}

	//register all events and start editor
	var start = function() {
		disableScreenFunc();
		//set link to git edit page address
		var link = getElementFromOptions('link');
		if(newpage){
			setAttribute(link, 'href', getNewPageInGitUrl());
		} else {
			setAttribute(link, 'href', getEditInGitUrl());
		}
		//set all button click handlers
		var loginButton = getElementFromOptions('loginButton');
		var logoutButton = getElementFromOptions('logoutButton');
		var commitButton = getElementFromOptions('commitButton');
		registerClickHandler(loginButton, function(){login();});
		registerClickHandler(logoutButton, function(){logout();});
		registerClickHandler(commitButton, function(){commit();});
		// Get elements that need to be hidden or shown depending on login status
		authenticatedElements = Array.prototype.slice.call(getElementsFromOptions('visibleAuthenticated')).map(getElementAndDisplayStyle)
		unauthenticatedElements = Array.prototype.slice.call(getElementsFromOptions('visibleUnauthenticated')).map(getElementAndDisplayStyle);
		// Get form elements
		usernameField = getElementFromOptions('usernameInput');
		passwordField = getElementFromOptions('passwordInput');
		stayloggedinCheckBox = getElementFromOptions('stayloggedin');
		commitMessageTextArea = getElementFromOptions('commitMessageTextArea');
		
		// By default hide elements that are only for authenticated users
		setVisibility(false)

		editPanel = getElementFromOptions('editPanel');
		editorOptions.element = editPanel;
		//start loading
		loadEditor();
		if(getUser() !== undefined){
			setVisibility(true);
		}
		enableScreenFunc();
	}
	
	return {
		start: start,
		loadEditor: loadEditor,
		getUser: getUser,
		login: login,
		logout: logout,
		getUrlPath: getUrlPath,
		editorOptions: editorOptions,
		uploadFile: uploadFile
	}	
}

module.exports = git_md_webedit;
