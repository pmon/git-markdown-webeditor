#!/bin/bash
preamble='//Licensed_under_GPL_3.0,_copyright_Philippe_Monnaie_2017._See_https://gitlab.com/pmon/git-markdown-webeditor'
rm -rf dist/*
browserify  --entry --standalone git_md_webedit src/git-md-webedit.js > dist/git-md-webedit.js
uglifyjs --compress --mangle --preamble $preamble -- dist/git-md-webedit.js > dist/git-md-webedit.min.js
cp -f dist/git-md-webedit.min.js examples/assets/js/git-markdown-webeditor/git-md-webedit.min.js
cp -f dist/git-md-webedit.js examples/assets/js/git-markdown-webeditor/git-md-webedit.js
