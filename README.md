# Gitlab-markdown-webeditor

gitlab-markdown-webeditor is a library that encapsulates functions to load markdown content from a git repository, edit it in WYSIWYG-esque editor and commit the changes. See examples on how to integrate this into your website. 

Currently still a work in progress (some functions are still in a TODO state). Very little has been well tested.

Currently needs jquery because of gitlab-api-js (planning to remove that dependency on jquery).

For license see {License.txt}

# TODO

 - Fix package.json
 - Add tests
 - Add functionality to see if the logged in user has commit rights (https://docs.gitlab.com/ce/api/members.html) 
 - Setup build process (bundle all js and css, minify, run tests)
 - Improve documentation (basic usage, options list)
   - Examples: 
     - test show public page with/without login
	 - test show non-public page with/without login
	 - test create new page with/without login
	 - test update page via fork / merge request / delete?
 - Maybe put toolbar in options and later insert into simpleMdeOptions for "simplicity" (though again it might make things more difficult for people that know simplemde...)
 - Add dist zipped file with file in nice dir structure
 - Bundle used css (currently using simplemde and bootstrap in examples, also check how it looks without).
 - Clean up code for making a new page vs editing a page (like a map of functions for each or something) to get rid of the random if/else blocks.
 - Add browser-like tests?
 - Add option to redirect after submitting edit or creation (redirect to edited or created page, though it may not be ready yet...)
 
# Dependencies
Uses:
 - [SimpleMDE](https://github.com/NextStepWebs/simplemde-markdown-editor) 
 - [js-cookie](https://github.com/js-cookie/js-cookie) 
 - [gitlab-api-js](https://gitlab.com/pmon/gitlab-api-js) 
 - Examples use bootstrap
 - Currently still depends on jquery